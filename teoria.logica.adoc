= Teoría Lógica

== Conjuntos

=== Noción de Conjunto

Una colección de objetos, no importa el *orden* ni las *repeticiones*.

===== Ejemplos de Conjuntos

                    ⎧ ℕ → Números naturales
                    ⎟ ℤ → Números Enteros
Conjuntos infinitos	⎨
                    ⎟ ℚ → Números Racionales
                    ⎩ ℝ → Números Reales

A=⎨a,b,c⎬ ⟵ es un conjunto finito
A=⎨b,c,a⎬
A=⎨a,a,b,c⎬
Los objetos que pertenecen al conjunto los llamamos elementos.

A=⎨a,a,1,3,5⎬

a∈A

∈=Pertenece
1∈A
0∉A 0	→ El 0 (cero) NO pertenece al conjunto A
B=⎨⎨1,2⎬,3,5,-2,z⎬
⎨1,2⎬ ∈ B
1∉B
C=⎨⎨1,2⎬,1,3,5,-2,z⎬
1∈C
Existen dos maneras de definir un conjunto por extensión y el otro por comprensión.

*NOTA*

Hasta ahora los que se dieron como ejemplo eran por extensión.

==== Definición

Cuando un conjunto es finito podemos definirlo por extensión. Esto es listar todos los elementos del conjunto.
Por ejemplo, los conjuntos A, B y C dados anteriormente están definidos por extensión.

==== Definición

Definir por comprensión es dar una propiedad que verifican los elementos y que además dicha propiedad la cumplen únicamente los elementos del conjunto.

Ejemplo: A= ⎨1,2,3⎬	⟵ por extensión

Dado - Conjunto Universal (𝕦)
A=⎨x ∈ ℕ / 1 ≤ x ≤ 3 ⎬
/= Tal que				ó
A=⎨x ∈ ℕ / x ≤ 3 ⎬		ó
A=⎨x ∈ ℕ / x < 4 ⎬		ó
A=⎨x ∈ ℤ / 1 ≤ x ≤ 3  ⎬

=== Diagramas de Venn

A= ⎨1,2,3⎬

Falta meter un gráfico.

==== Ejercicio

A= ⎨1,2,3,4⎬
B= ⎨3,4,5,6,7,8,9⎬
Describir por extensión el conjunto C= ⎨x ∈ A /  ∈  B⎬

=== Respuesta

C= ⎨2,3⎬

=== Inclusión de conjuntos

A⊂B ⟵ A incluido en B
Decimos que A está incluido en B, si todos los elementos de A pertenecen a B.

Falta un gráfico de inclusión
